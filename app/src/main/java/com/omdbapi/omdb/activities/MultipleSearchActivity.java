package com.omdbapi.omdb.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.omdbapi.omdb.utils.ConnectionDetector;
import com.omdbapi.omdb.utils.Constants;
import com.omdbapi.omdb.utils.DatabaseHandler;
import com.omdbapi.omdb.R;
import com.omdbapi.omdb.modal.SectionDataModel;
import com.omdbapi.omdb.adapters.MultipleSearchDataAdapter;
import com.omdbapi.omdb.modal.pojo.Search;
import com.omdbapi.omdb.modal.pojo.SearchResult;
import com.omdbapi.omdb.modal.pojo.TitleIdResult;
import com.omdbapi.omdb.presenter.MainActivityPresenter;
import com.omdbapi.omdb.presenter.TitleIdResultPresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MultipleSearchActivity extends AppCompatActivity {

    private Toolbar toolbar;
    RecyclerView recyclerView;
    MultipleSearchDataAdapter adapter;
    ArrayList<SectionDataModel> allSampleData;
    protected Handler handler;
    TitleIdResultPresenter titleIdResultPresenter;
    DatabaseHandler databaseHandler;
    View view;
    ArrayList<String> items;
    MainActivityPresenter mainActivityPresenter;
    TextView emptyTextView;
    SectionDataModel sectionDataModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_search);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        databaseHandler = DatabaseHandler.getHandler(this);
        titleIdResultPresenter = new TitleIdResultPresenter();
        mainActivityPresenter = new MainActivityPresenter();
        allSampleData = new ArrayList<SectionDataModel>();
        emptyTextView = (TextView) findViewById(R.id.empty_view);

        items = ((ArrayList<String>) getIntent().getSerializableExtra("items"));
        handler = new Handler();
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setTitle("Search");

        }

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new MultipleSearchDataAdapter(this, allSampleData);
        recyclerView.setAdapter(adapter);
        createData(((SearchResult) getIntent().getSerializableExtra("search")));
    }


    public void onClicked(Search movie, View imageView) {
        view = imageView;
        TitleIdResult movies = databaseHandler.getMovie(movie.getImdbID());
        if (movies != null) {
            if (ConnectionDetector.isConnectedToInternet(this)) {
                if (System.currentTimeMillis() - Long.parseLong(movies.getUpadatedAt()) > Constants.getTimeinMillis(1)) {
                    titleIdResultPresenter.callPresenter(this, "i", movie.getImdbID(), movie.getType(), true);
                } else {
                    openDetailActivity(movies);
                    Log.e("Data", movies.toString());
                }
            } else {
                openDetailActivity(movies);
                Log.e("Data", movies.toString());
            }
        } else {
            titleIdResultPresenter.callPresenter(this, "i", movie.getImdbID(), movie.getType(), false);
        }
    }

    public void addData(TitleIdResult data) {
        if (data.getError().equals("noError")) {
            databaseHandler.addContact(data);
            openDetailActivity(data);
        } else {
            Toast.makeText(this, "There may be no data. Or please retry.", Toast.LENGTH_LONG).show();
        }
    }


    public void updateData(TitleIdResult data) {
        if (data.getError().equals("noError")) {
            databaseHandler.updateMovie(data);
            openDetailActivity(data);
        } else {
            Toast.makeText(this, "There may be no data. Or please retry.", Toast.LENGTH_LONG).show();
        }
    }

    private void openDetailActivity(TitleIdResult movies) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("movies", movies);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Pair<View, String> pair = Pair.create(view, "image");
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pair);
            ActivityCompat.startActivity(this, intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    public void createData(SearchResult search) {
        if (search.getError().equals("noError")) {
            SectionDataModel dm = new SectionDataModel();
            dm.setHeaderTitle(items.get(0));
            dm.setTotalCout(search.getTotalResults());
            dm.setType(getIntent().getStringExtra("type"));
            List<Search> singleItem = new ArrayList<>();
            singleItem = Arrays.asList(search.getSearch());
            dm.setAllItemsInSection(singleItem);
            allSampleData.add(dm);
            adapter.notifyDataSetChanged();
            adapter.setLoaded();
        }
        items.remove(0);
        if (items.size() > 0) {
            adapter.loading = true;
            allSampleData.add(null);
            adapter.notifyItemInserted(allSampleData.size() - 1);
            mainActivityPresenter.callPresenterWithoutLoader(this, "s", items.get(0), "", "", "");
        } else {
            if (recyclerView.getAdapter().getItemCount() < 1) {
                recyclerView.setVisibility(View.GONE);
                emptyTextView.setVisibility(View.VISIBLE);
            }
        }
    }


    public void load(SearchResult data) {
        allSampleData.remove(allSampleData.size() - 1);
        adapter.notifyItemRemoved(allSampleData.size());
        createData(data);
    }

    public void onMoreClicked(SectionDataModel sectionDataModel) {
        this.sectionDataModel = sectionDataModel;
        mainActivityPresenter.callPresenter(this, "s", sectionDataModel.getHeaderTitle(), sectionDataModel.getType(), "", "2", null);
    }

    public void setAdapters(SearchResult data, String type, String key) {
        List<Search> list = new ArrayList<Search>();
        list.addAll(sectionDataModel.getAllItemsInSection());
        list.addAll(Arrays.asList(data.getSearch()));
        data.setSearch(list.toArray(new Search[list.size()]));
        if (data.getError().equals("noError")) {
            Intent intent = new Intent(this, GridActivity.class);
            intent.putExtra("search", data);
            intent.putExtra("page", 2);
            intent.putExtra("key", key);
            intent.putExtra("type", type);
            startActivity(intent);
        } else {
            Toast.makeText(this, "No Records", Toast.LENGTH_LONG).show();
        }
    }
}
