package com.omdbapi.omdb.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.omdbapi.omdb.listeners.OnLoadMoreListener;
import com.omdbapi.omdb.presenter.MainActivityPresenter;
import com.omdbapi.omdb.utils.ConnectionDetector;
import com.omdbapi.omdb.utils.Constants;
import com.omdbapi.omdb.utils.DatabaseHandler;
import com.omdbapi.omdb.R;
import com.omdbapi.omdb.adapters.MovieAdapter;
import com.omdbapi.omdb.modal.pojo.Search;
import com.omdbapi.omdb.modal.pojo.SearchResult;
import com.omdbapi.omdb.modal.pojo.TitleIdResult;
import com.omdbapi.omdb.presenter.TitleIdResultPresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GridActivity extends AppCompatActivity {

    private MovieAdapter adapter;
    TitleIdResultPresenter titleIdResultPresenter;
    DatabaseHandler databaseHandler;
    private View view;
    private View textView;
    List<Search> movieList;
    MainActivityPresenter mainActivityPresenter;
    int page;
    String key;
    String type;
    GridActivity gridActivity;
    private boolean load = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        gridActivity = this;
        setSupportActionBar(toolbar);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        databaseHandler = DatabaseHandler.getHandler(this);
        titleIdResultPresenter = new TitleIdResultPresenter();
        mainActivityPresenter = new MainActivityPresenter();
        page = getIntent().getIntExtra("page", 1);
        key = getIntent().getStringExtra("key");
        type = getIntent().getStringExtra("type");
        SearchResult searchResult = (SearchResult) getIntent().getSerializableExtra("search");
        movieList = new ArrayList<>(Arrays.asList(searchResult.getSearch()));
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new MovieAdapter(this, movieList, recyclerView);
//        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (load) {
                    movieList.add(null);
                    adapter.notifyItemInserted(movieList.size() - 1);
                    page += 1;
                    mainActivityPresenter.callPresenter(gridActivity, "s", key, type, "", String.valueOf(page), null);
                }
            }
        });
    }

    public void setAdapters(SearchResult data) {
        movieList.remove(movieList.size() - 1);
        adapter.notifyItemRemoved(movieList.size());
        if (data.getError().equalsIgnoreCase("noError")) {
            for (Search search : data.getSearch()) {
                movieList.add(search);
                adapter.notifyItemInserted(movieList.size());
            }
            adapter.setLoaded();
        } else {
            page -= 1;
            load = false;
        }
    }

    public void onClicked(Search movie, View imageView, View textView) {
        view = imageView;
        this.textView = textView;
        TitleIdResult movies = databaseHandler.getMovie(movie.getImdbID());
        if (movies != null) {
            if (ConnectionDetector.isConnectedToInternet(this)) {
                if (System.currentTimeMillis() - Long.parseLong(movies.getUpadatedAt()) > Constants.getTimeinMillis(1)) {
                    titleIdResultPresenter.callPresenter(this, "i", movie.getImdbID(), movie.getType(), true);
                } else {
                    openDetailActivity(movies);
                    Log.e("Data", movies.toString());
                }
            } else {
                openDetailActivity(movies);
                Log.e("Data", movies.toString());
            }
        } else {
            titleIdResultPresenter.callPresenter(this, "i", movie.getImdbID(), movie.getType(), false);
        }
    }

    public void addData(TitleIdResult data) {
        if (data.getError().equals("noError")) {
            databaseHandler.addContact(data);
            openDetailActivity(data);
        } else {
            Toast.makeText(this, "There may be no data. Or please retry.", Toast.LENGTH_LONG).show();
        }
    }

    public void updateData(TitleIdResult data) {
        if (data.getError().equals("noError")) {
            databaseHandler.updateMovie(data);
            openDetailActivity(data);
        } else {
            Toast.makeText(this, "There may be no data. Or please retry.", Toast.LENGTH_LONG).show();
        }
    }

    private void openDetailActivity(TitleIdResult movies) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("movies", movies);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Pair<View, String> pair1 = Pair.create(view, "image");
            Pair<View, String> pair2 = Pair.create(textView, "text");
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pair1, pair2);
            ActivityCompat.startActivity(this, intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}