package com.omdbapi.omdb.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration;
import com.beloo.widget.chipslayoutmanager.gravity.IChildGravityResolver;
import com.omdbapi.omdb.modal.pojo.TitleIdResult;
import com.omdbapi.omdb.utils.ConnectionDetector;
import com.omdbapi.omdb.utils.DatabaseHandler;
import com.omdbapi.omdb.R;
import com.omdbapi.omdb.adapters.ChipsAdapter;
import com.omdbapi.omdb.listeners.OnAddListener;
import com.omdbapi.omdb.listeners.OnRemoveListener;
import com.omdbapi.omdb.modal.pojo.SearchResult;
import com.omdbapi.omdb.presenter.MainActivityPresenter;

import java.util.ArrayList;

import info.hoang8f.android.segmented.SegmentedGroup;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, OnRemoveListener, OnAddListener, View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    Toolbar toolbar;
    AppCompatTextView searchText;
    RecyclerView recyclerView;
    ChipsAdapter adapter;
    private ArrayList<String> items;
    private static final String EXTRA = "data";
    SearchManager searchManager;
    SearchView searchView;
    SegmentedGroup search, type;
    RadioButton normal, imdb, title, movies, tv, none;
    AppCompatButton searchButton;
    MainActivityPresenter mainActivityPresenter;
    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        searchText = (AppCompatTextView) findViewById(R.id.search_text);
        search = (SegmentedGroup) findViewById(R.id.search_radio_group);
        type = (SegmentedGroup) findViewById(R.id.type_radio_group);
        normal = (RadioButton) findViewById(R.id.normal_radio_button);
        imdb = (RadioButton) findViewById(R.id.imdb_radio_button);
        title = (RadioButton) findViewById(R.id.title_radio_button);
        movies = (RadioButton) findViewById(R.id.movies_radio_button);
        tv = (RadioButton) findViewById(R.id.tv_radio_button);
        none = (RadioButton) findViewById(R.id.none_radio_button);
        searchButton = (AppCompatButton) findViewById(R.id.search_movie_button);
        setSupportActionBar(toolbar);
        databaseHandler = DatabaseHandler.getHandler(this);
        mainActivityPresenter = new MainActivityPresenter();
        search.setOnCheckedChangeListener(this);

        searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.search_view);
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
            searchView.setOnQueryTextListener(this);
        }

        items = new ArrayList<>();

        ChipsLayoutManager chipsLayoutManager = ChipsLayoutManager.newBuilder(this)
                .setChildGravity(Gravity.TOP)
                .setScrollingEnabled(true)
                .setMaxViewsInRow(5)
                .setGravityResolver(new IChildGravityResolver() {
                    @Override
                    public int getItemGravity(int position) {
                        return Gravity.CENTER;
                    }
                })
                .setOrientation(ChipsLayoutManager.HORIZONTAL)
                .setRowStrategy(ChipsLayoutManager.STRATEGY_FILL_SPACE)
                .withLastRow(true)
                .build();

        recyclerView.addItemDecoration(new SpacingItemDecoration(getResources().getDimensionPixelOffset(R.dimen.item_space),
                getResources().getDimensionPixelOffset(R.dimen.item_space)));
        adapter = new ChipsAdapter(items, this);
        recyclerView.setLayoutManager(chipsLayoutManager);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 10);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(1, 10);
        recyclerView.setAdapter(adapter);

        normal.setChecked(true);
        none.setChecked(true);
        searchButton.setOnClickListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (!items.contains(query)) {
            items.add(query);
            searchView.setQuery("", false);
            onItemAdd(items.size() - 1);
        } else {
            Toast.makeText(this, "Query Already Added", Toast.LENGTH_LONG).show();
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onItemRemoved(int position) {
        items.remove(position);
        adapter.notifyItemRemoved(position);
    }

    @Override
    public void onItemAdd(int position) {
        adapter.notifyItemInserted(position);
    }

    @Override
    public void onClick(View v) {
        if (items.size() > 0) {
            String s, t, key;
            switch (search.getCheckedRadioButtonId()) {
                case R.id.normal_radio_button:
                    s = "s";
                    break;
                case R.id.imdb_radio_button:
                    s = "i";
                    break;
                case R.id.title_radio_button:
                    s = "t";
                    break;
                default:
                    s = "s";
                    break;
            }

            switch (type.getCheckedRadioButtonId()) {
                case R.id.movies_radio_button:
                    t = "movie";
                    break;
                case R.id.tv_radio_button:
                    t = "series";
                    break;
                case R.id.none_radio_button:
                    t = "";
                    break;
                default:
                    t = "";
                    break;
            }

            key = items.get(0);
            if (ConnectionDetector.isConnectedToInternet(this)) {
                mainActivityPresenter.callPresenter(this, s, key, t, "", "", items);
            } else {
//            List<TitleIdResult> result = databaseHandler.getMovieOffline(s, items.get(0).getName(), t, "", "");
//            result.toArray(new T)
//            if ()
            }
        } else {
            Toast.makeText(this, "No Search  Query Added", Toast.LENGTH_LONG).show();
        }
    }

    public void setAdapters(SearchResult data, String type, String key) {
        if (data.getError().equals("noError")) {
            Intent intent = new Intent(this, GridActivity.class);
            intent.putExtra("search", data);
            intent.putExtra("page", 1);
            intent.putExtra("key", key);
            intent.putExtra("type", type);
            startActivity(intent);
        } else {
            Toast.makeText(this, "No Records", Toast.LENGTH_LONG).show();
        }
    }

    public void setMultiple(SearchResult data, ArrayList<String> items, String type) {
        Intent intent = new Intent(this, MultipleSearchActivity.class);
        intent.putExtra("search", data);
        intent.putExtra("items", items);
        intent.putExtra("type", type);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        if(R.id.normal_radio_button == checkedId) {
            adapter.setStrike(false);
            adapter.notifyDataSetChanged();
        } else {
            adapter.setStrike(true);
            adapter.notifyDataSetChanged();
        }
    }

    public void setAdaptersId(TitleIdResult dataId) {
        if (dataId.getError().equalsIgnoreCase("noError")) {
            Intent intent = new Intent(this, DetailsActivity.class);
            intent.putExtra("movies", dataId);
            startActivity(intent);
        } else {
            Toast.makeText(this, "No Records", Toast.LENGTH_LONG).show();
        }
    }
}


//    final Dialog dialog = new Dialog(this);
//    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//    dialog.setCancelable(false);
//    dialog.setCanceledOnTouchOutside(false);
//    dialog.setContentView(R.layout.custom_dialog);
//    TextView btnDone = (TextView) dialog.findViewById(R.id.custom_dialog_btn_done);
//    btnDone.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            dialog.dismiss();
//        }
//    });
//    dialog.show();
