package com.omdbapi.omdb.activities;

import android.os.Build;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.omdbapi.omdb.R;
import com.omdbapi.omdb.adapters.DetailAdapter;
import com.omdbapi.omdb.modal.pojo.TitleIdResult;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    DetailAdapter detailAdapter;
    TitleIdResult movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        movie = (TitleIdResult) getIntent().getSerializableExtra("movies");
        getSupportActionBar().setTitle(movie.getTitle());
        Picasso.with(this).load(movie.getPoster()).placeholder(R.drawable.album9).error(R.drawable.album1).into(imageView);
        supportPostponeEnterTransition();
        imageView.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                        toolbar.getViewTreeObserver().removeOnPreDrawListener(this);
                        supportStartPostponedEnterTransition();
                        return true;
                    }
                }
        );
        List<Pair<String, String>> data = new ArrayList<>();

        if (!movie.getPlot().isEmpty() && !movie.getPlot().equals("N/A")) {
            data.add(Pair.create("Plot", movie.getPlot()));
        }
        if (!movie.getYear().isEmpty() && !movie.getYear().equals("N/A")) {
            data.add(Pair.create("Year", movie.getYear()));
        }
        if (!movie.getRated().isEmpty() && !movie.getRated().equals("N/A")) {
            data.add(Pair.create("Rated", movie.getRated()));
        }
        if (!movie.getReleased().isEmpty() && !movie.getReleased().equals("N/A")) {
            data.add(Pair.create("Released", movie.getReleased()));
        }
        if (!movie.getSeason().isEmpty() && !movie.getSeason().equals("N/A")) {
            data.add(Pair.create("Season", movie.getSeason()));
        }
        if (!movie.getTotalSeasons().isEmpty() && !movie.getTotalSeasons().equals("N/A")) {
            data.add(Pair.create("Seasons", movie.getTotalSeasons()));
        }
        if (!movie.getEpisode().isEmpty() && !movie.getEpisode().equals("N/A")) {
            data.add(Pair.create("Episode", movie.getEpisode()));
        }
        if (!movie.getRuntime().isEmpty() && !movie.getRuntime().equals("N/A")) {
            data.add(Pair.create("Runtime", movie.getRuntime()));
        }
        if (!movie.getGenre().isEmpty() && !movie.getGenre().equals("N/A")) {
            data.add(Pair.create("Genre", movie.getGenre()));
        }
        if (!movie.getDirector().isEmpty() && !movie.getDirector().equals("N/A")) {
            data.add(Pair.create("Director", movie.getDirector()));
        }
        if (!movie.getWriter().isEmpty() && !movie.getWriter().equals("N/A")) {
            data.add(Pair.create("Writer", movie.getWriter()));
        }
        if (!movie.getActors().isEmpty() && !movie.getActors().equals("N/A")) {
            data.add(Pair.create("Actors", movie.getActors()));
        }
        if (!movie.getLanguage().isEmpty() && !movie.getLanguage().equals("N/A")) {
            data.add(Pair.create("Language", movie.getLanguage()));
        }
        if (!movie.getCountry().isEmpty() && !movie.getCountry().equals("N/A")) {
            data.add(Pair.create("Country", movie.getCountry()));
        }
        if (!movie.getAwards().isEmpty() && !movie.getAwards().equals("N/A")) {
            data.add(Pair.create("Awards", movie.getAwards()));
        }
        if (!movie.getMetascore().isEmpty() && !movie.getMetascore().equals("N/A")) {
            data.add(Pair.create("Metascore", movie.getMetascore()));
        }
        if (!movie.getImdbRating().isEmpty() && !movie.getImdbRating().equals("N/A")) {
            data.add(Pair.create("IMDB Rating", movie.getImdbRating()));
        }
        if (!movie.getImdbVotes().isEmpty() && !movie.getImdbVotes().equals("N/A")) {
            data.add(Pair.create("IMDB Votes", movie.getImdbVotes()));
        }
        if (!movie.getSeriesID().isEmpty() && !movie.getSeriesID().equals("N/A")) {
            data.add(Pair.create("Series ID", movie.getSeriesID()));
        }
        if (!movie.getType().isEmpty() && !movie.getType().equals("N/A")) {
            data.add(Pair.create("Type", movie.getType()));
        }

        recyclerView = (RecyclerView) findViewById(R.id.details_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        detailAdapter = new DetailAdapter(this, data);
        recyclerView.setAdapter(detailAdapter);
    }

    public void onClicked() {

    }
}
