package com.omdbapi.omdb.adapters;

import android.content.Context;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.omdbapi.omdb.R;
import com.omdbapi.omdb.activities.DetailsActivity;

import java.util.List;

/**
 * Created by jeet on 25-03-2017.
 */

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.MyViewHolder> {

    private Context mContext;
    List<Pair<String, String>> movie;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView header, detail;
        CardView cardView;
        ImageView arrow;

        MyViewHolder(View view) {
            super(view);
            header = (TextView) view.findViewById(R.id.header);
            detail = (TextView) view.findViewById(R.id.detail);
            arrow = (ImageView) view.findViewById(R.id.arrow);
            cardView = (CardView) view.findViewById(R.id.details_card_view);
        }
    }

    public DetailAdapter(Context mContext, List<Pair<String, String>> movie) {
        this.mContext = mContext;
        this.movie = movie;
    }

    @Override
    public DetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.details_item_card, parent, false);
        return new DetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DetailAdapter.MyViewHolder holder, int position) {
        holder.header.setText(movie.get(position).first);
        holder.detail.setText(movie.get(position).second);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DetailsActivity) mContext).onClicked();
            }
        };
        if ((movie.get(position).first.equals("Seasons"))) {
            holder.arrow.setVisibility(View.VISIBLE);
            holder.cardView.setOnClickListener(onClickListener);
        } else {
            holder.arrow.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return movie.size();
    }
}