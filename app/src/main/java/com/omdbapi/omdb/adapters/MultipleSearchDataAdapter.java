package com.omdbapi.omdb.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.omdbapi.omdb.R;
import com.omdbapi.omdb.activities.MultipleSearchActivity;
import com.omdbapi.omdb.modal.SectionDataModel;
import com.omdbapi.omdb.presenter.MainActivityPresenter;
import com.omdbapi.omdb.presenter.TitleIdResultPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeet on 24-03-2017.
 */

public class MultipleSearchDataAdapter extends RecyclerView.Adapter {

    private ArrayList<SectionDataModel> dataList;
    private Context mContext;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    public boolean loading;

    public MultipleSearchDataAdapter(Context context, ArrayList<SectionDataModel> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        return dataList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.multiple_list_item, viewGroup, false);
            vh = new ItemRowHolder(v);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.progress_bar_item, viewGroup, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder itemRowHolder, final int i) {
        if (itemRowHolder instanceof ItemRowHolder) {
            final String sectionName = dataList.get(i).getHeaderTitle();
            List singleSectionItems = dataList.get(i).getAllItemsInSection();
            ((ItemRowHolder) itemRowHolder).itemTitle.setText(sectionName);
            SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, singleSectionItems);
            ((ItemRowHolder) itemRowHolder).recycler_view_list.setHasFixedSize(true);
            ((ItemRowHolder) itemRowHolder).recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            ((ItemRowHolder) itemRowHolder).recycler_view_list.setAdapter(itemListDataAdapter);
            ((ItemRowHolder) itemRowHolder).btnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MultipleSearchActivity) mContext).onMoreClicked(dataList.get(i));
                }
            });
        } else {
            ((ProgressViewHolder) itemRowHolder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public void setLoaded() {
        loading = false;
    }

    private class ItemRowHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        RecyclerView recycler_view_list;
        Button btnMore;

        ItemRowHolder(View view) {
            super(view);
            this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
            this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            this.btnMore = (Button) view.findViewById(R.id.btnMore);
        }
    }

    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}