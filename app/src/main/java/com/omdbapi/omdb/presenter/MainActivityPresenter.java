package com.omdbapi.omdb.presenter;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.omdbapi.omdb.activities.GridActivity;
import com.omdbapi.omdb.modal.pojo.TitleIdResult;
import com.omdbapi.omdb.utils.ConnectionDetector;
import com.omdbapi.omdb.utils.Constants;
import com.omdbapi.omdb.utils.Dialogs;
import com.omdbapi.omdb.activities.MainActivity;
import com.omdbapi.omdb.activities.MultipleSearchActivity;
import com.omdbapi.omdb.modal.pojo.SearchResult;
import com.omdbapi.omdb.modal.restclient.RestClient;

import java.io.Serializable;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 06-08-2016.
 */
public class MainActivityPresenter {
    private AppCompatActivity context;
    private SearchResult data;
    private TitleIdResult dataId;
    ArrayList<String> items;
    String t, key;

    private void getIdResut(String key, String type, String year) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            final ProgressDialog d = Dialogs.showLoading(context);
            d.setCanceledOnTouchOutside(false);
            Call<TitleIdResult> call = RestClient.get().getIdResult(key, type, year, "", "");
            call.enqueue(new Callback<TitleIdResult>() {
                @Override
                public void onResponse(Call<TitleIdResult> call, Response<TitleIdResult> response) {
                    if (response.isSuccessful()) {
                        TitleIdResult basePojo = response.body();
                        dataId = basePojo;
                        publishId(context);

                    } else {
                        int statusCode = response.code();
                        // handle request errors yourself
                        ResponseBody errorBody = response.errorBody();
                        Dialogs.showMessage(context, errorBody.toString());
                    }
                    d.dismiss();
                }

                @Override
                public void onFailure(Call<TitleIdResult> call, Throwable t) {
                    d.dismiss();
                    Toast.makeText(context, Constants.SOMETHING_WRONG, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Dialogs.showMessage(context, Constants.CHECK_CONNECTION_FALSE);
        }
    }

    private void getTitleResut(String key, String type, String year) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            final ProgressDialog d = Dialogs.showLoading(context);
            d.setCanceledOnTouchOutside(false);
            Call<TitleIdResult> call = RestClient.get().getTitleResult(key, type, year, "", "");
            call.enqueue(new Callback<TitleIdResult>() {
                @Override
                public void onResponse(Call<TitleIdResult> call, Response<TitleIdResult> response) {
                    if (response.isSuccessful()) {
                        TitleIdResult basePojo = response.body();
                        dataId = basePojo;
                        publishId(context);

                    } else {
                        int statusCode = response.code();
                        // handle request errors yourself
                        ResponseBody errorBody = response.errorBody();
                        Dialogs.showMessage(context, errorBody.toString());
                    }
                    d.dismiss();
                }

                @Override
                public void onFailure(Call<TitleIdResult> call, Throwable t) {
                    d.dismiss();
                    Toast.makeText(context, Constants.SOMETHING_WRONG, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Dialogs.showMessage(context, Constants.CHECK_CONNECTION_FALSE);
        }
    }

    private void getSearchResult(String key, String type, String year, String page) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            final ProgressDialog d = Dialogs.showLoading(context);
            d.setCanceledOnTouchOutside(false);
            Call<SearchResult> call = RestClient.get().getSearchResult(key, type, year, page);
            call.enqueue(new Callback<SearchResult>() {
                @Override
                public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                    if (response.isSuccessful()) {
                        SearchResult basePojo = response.body();
                        data = basePojo;
                        publish(context);

                    } else {
                        int statusCode = response.code();
                        // handle request errors yourself
                        ResponseBody errorBody = response.errorBody();
                        Dialogs.showMessage(context, errorBody.toString());
                    }
                    d.dismiss();
                }

                @Override
                public void onFailure(Call<SearchResult> call, Throwable t) {
                    d.dismiss();
                    Toast.makeText(context, Constants.SOMETHING_WRONG, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Dialogs.showMessage(context, Constants.CHECK_CONNECTION_FALSE);
        }
    }

    private void getSearchResultWithoutLoader(String key, String type, String year, String page) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            Call<SearchResult> call = RestClient.get().getSearchResult(key, type, year, page);
            call.enqueue(new Callback<SearchResult>() {
                @Override
                public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                    if (response.isSuccessful()) {
                        SearchResult basePojo = response.body();
                        data = basePojo;
                        publishWithoutLoader(context);

                    } else {
                        int statusCode = response.code();
                        // handle request errors yourself
                        ResponseBody errorBody = response.errorBody();
                        Dialogs.showMessage(context, errorBody.toString());
                    }
                }

                @Override
                public void onFailure(Call<SearchResult> call, Throwable t) {
                    Toast.makeText(context, Constants.SOMETHING_WRONG, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Dialogs.showMessage(context, Constants.CHECK_CONNECTION_FALSE);
        }
    }

    public void callPresenter(AppCompatActivity context, String search, String key, String type, String year, String page, ArrayList<String> items) {
        this.context = context;
        this.items = items;
        this.t = type;
        this.key = key;
        switch (search) {
            case "s":
                getSearchResult(key, type, year, page);
                break;
            case "i":
                getIdResut(key, type, year);
                break;
            case "t":
                getTitleResut(key, type, year);
                break;
            default:
                getSearchResult(key, type, year, page);
                break;
        }

    }

    public void callPresenterWithoutLoader(AppCompatActivity context, String search, String key, String type, String year, String page) {
        this.context = context;
        getSearchResultWithoutLoader(key, type, year, page);
    }

    private void publish(AppCompatActivity view) {
        if (view != null) {
            if (view.getLocalClassName().equals("MainActivity") || view.getLocalClassName().equals("activities.MainActivity")) {
                if (data != null) {
                    if (items.size() > 1) {
                        ((MainActivity) view).setMultiple(data, items, t);
                    } else {
                        ((MainActivity) view).setAdapters(data, t, key);
                    }
                }
            } else if (view.getLocalClassName().equals("MultipleSearchActivity") || view.getLocalClassName().equals("activities.MultipleSearchActivity")) {
                if (data != null) {
                    ((MultipleSearchActivity) view).setAdapters(data, t, key);
                }
            } else if (view.getLocalClassName().equals("GridActivity") || view.getLocalClassName().equals("activities.GridActivity")) {
                if (data != null) {
                    ((GridActivity) view).setAdapters(data);
                }
            }
        }
    }

    private void publishId(AppCompatActivity view) {
        if (view != null) {
            if (data != null) {
                ((MainActivity) view).setAdaptersId(dataId);
            }
        }
    }

    private void publishWithoutLoader(AppCompatActivity view) {
        if (view != null) {
            if (data != null) {
                ((MultipleSearchActivity) view).load(data);
            }
        }
    }
}
