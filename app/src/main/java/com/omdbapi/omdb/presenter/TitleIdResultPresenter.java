package com.omdbapi.omdb.presenter;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.omdbapi.omdb.utils.ConnectionDetector;
import com.omdbapi.omdb.utils.Constants;
import com.omdbapi.omdb.utils.Dialogs;
import com.omdbapi.omdb.activities.GridActivity;
import com.omdbapi.omdb.activities.MultipleSearchActivity;
import com.omdbapi.omdb.modal.pojo.TitleIdResult;
import com.omdbapi.omdb.modal.restclient.RestClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 06-08-2016.
 */
public class TitleIdResultPresenter {
    private AppCompatActivity context;
    private TitleIdResult data;
    boolean update;

    private void getIdResult(String key, String type) {
        if (ConnectionDetector.isConnectedToInternet(context)) {
            final ProgressDialog d = Dialogs.showLoading(context);
            d.setCanceledOnTouchOutside(false);
            Call<TitleIdResult> call = RestClient.get().getIdResult(key, type, "", "", "");
            call.enqueue(new Callback<TitleIdResult>() {
                @Override
                public void onResponse(Call<TitleIdResult> call, Response<TitleIdResult> response) {
                    if (response.isSuccessful()) {
                        TitleIdResult basePojo = response.body();
                        data = basePojo;
                        publish(context);

                    } else {
                        int statusCode = response.code();
                        // handle request errors yourself
                        ResponseBody errorBody = response.errorBody();
                        Dialogs.showMessage(context, errorBody.toString());
                    }
                    d.dismiss();
                }

                @Override
                public void onFailure(Call<TitleIdResult> call, Throwable t) {
                    d.dismiss();
                    Toast.makeText(context, Constants.SOMETHING_WRONG, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Dialogs.showMessage(context, Constants.CHECK_CONNECTION_FALSE);
        }
    }

    public void callPresenter(AppCompatActivity context, String search, String key, String type, boolean update) {
        this.update = update;
        this.context = context;
        switch (search) {
            case "i":
                getIdResult(key, type);
                break;
            case "t":
                break;
            default:
                getIdResult(key, type);
                break;
        }

    }

    public void publish(AppCompatActivity view) {
        if (view != null) {
            Log.e("Class Name", view.getLocalClassName());
            if (view.getLocalClassName().equals("MultipleSearchActivity") || view.getLocalClassName().equals("activities.MultipleSearchActivity")) {
                if (data != null) {
                    if (update) {
                        ((MultipleSearchActivity) view).updateData(data);
                    } else {
                        ((MultipleSearchActivity) view).addData(data);
                    }
                }
            } else if (view.getLocalClassName().equals("GridActivity") || view.getLocalClassName().equals("activities.GridActivity")) {
                if (data != null) {
                    if (update) {
                        ((GridActivity) view).updateData(data);
                    } else {
                        ((GridActivity) view).addData(data);
                    }
                }
            }
        }
    }
}
