package com.omdbapi.omdb.utils;

import android.app.Application;

/**
 * Created by jeet on 23-03-2017.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseHandler databaseHandler = DatabaseHandler.getHandler(getApplicationContext());
        databaseHandler.deleteViewedAt();
    }
}
