package com.omdbapi.omdb.utils;

/**
 * Created by Sameer on 7/9/2016.
 */
public class Constants {
    public static String ERROR_MESSAGE = "Something went wrong, try again later.";
    public static String LOADING_MESSAGE = "Loading...";
    public static String SOMETHING_WRONG = "May be your internet connection is too slow. Or Something went wrong with server. Try after some time.";
    public static String CHECK_CONNECTION_FALSE = "No internet connection.";
    public static double getTimeinMillis(double min) {
        return min * 60 * 1000;
    }

    public static double getTimeinMillisFromDay(double day) {
        return day * 24 * 60 * 60 * 1000;
    }
}
