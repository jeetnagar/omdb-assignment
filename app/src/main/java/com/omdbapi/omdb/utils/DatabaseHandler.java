package com.omdbapi.omdb.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.omdbapi.omdb.modal.pojo.TitleIdResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeet on 23-03-2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version

    private static DatabaseHandler instance;

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "omdbManager";

    // Contacts table name
    private static final String TABLE_NAME = "omdbtable";

    private static final String IMDBID_INDEX = "omdbtable_imbdid_imdbidx";

    private static final String KEY_ID = "id";

    private static final String TITLE = "title";

    private static final String YEAR = "year";

    private static final String RATED = "rated";

    private static final String RELEASED = "released";

    private static final String SEASON = "season";

    private static final String EPISODE = "episode";

    private static final String RUNTIME = "runtime";

    private static final String GENRE = "genre";

    private static final String DIRECTOR = "director";

    private static final String WRITER = "write";

    private static final String ACTORS = "actors";

    private static final String PLOT = "plot";

    private static final String LANGUAGE = "language";

    private static final String COUNTRY = "country";

    private static final String AWARDS = "awards";

    private static final String POSTER = "poster";

    private static final String METASCORE = "metascore";

    private static final String IMDBRATING = "imdbrating";

    private static final String IMDBVOTES = "imdbvotes";

    private static final String IMDBID = "imdbid";

    private static final String SERIESID = "seriesid";

    private static final String TYPE = "type";

    private static final String TOTALSEASONS = "totalseasons";

    private static final String UPDATEDAT = "updatedat";

    private static final String VIEWEDAT = "viewedat";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TITLE + " TEXT,"
                + YEAR + " TEXT," + RATED + " TEXT,"
                + RELEASED + " TEXT," + SEASON + " TEXT,"
                + EPISODE + " TEXT," + RUNTIME + " TEXT,"
                + GENRE + " TEXT," + DIRECTOR + " TEXT,"
                + WRITER + " TEXT," + ACTORS + " TEXT,"
                + PLOT + " TEXT," + LANGUAGE + " TEXT,"
                + COUNTRY + " TEXT," + AWARDS + " TEXT,"
                + POSTER + " TEXT," + METASCORE + " TEXT,"
                + IMDBRATING + " TEXT," + IMDBVOTES + " TEXT,"
                + IMDBID + " TEXT," + SERIESID + " TEXT,"
                + TYPE + " TEXT," + TOTALSEASONS + " TEXT,"
                + UPDATEDAT + " INTEGER," + VIEWEDAT + " INTEGER"
                + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
        String createIMDBIdIndex = "CREATE INDEX " + IMDBID_INDEX + " ON " + TABLE_NAME + "(" + IMDBID + ")";
        db.execSQL(createIMDBIdIndex);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public static synchronized DatabaseHandler getHandler(Context context) {
        if (instance == null) instance = new DatabaseHandler(context);
        return instance;
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void addContact(TitleIdResult titleResult) {
        SQLiteDatabase db = this.getWritableDatabase();
        titleResult.setUpadatedAt(String.valueOf(System.currentTimeMillis()));
        titleResult.setViewedAt(String.valueOf(System.currentTimeMillis()));
        ContentValues values = new ContentValues();
        values.put(TITLE, titleResult.getTitle());
        values.put(YEAR, titleResult.getYear());
        values.put(RATED, titleResult.getRated());
        values.put(RELEASED, titleResult.getReleased());
        values.put(SEASON, titleResult.getSeason());
        values.put(EPISODE, titleResult.getEpisode());
        values.put(RUNTIME, titleResult.getRuntime());
        values.put(GENRE, titleResult.getGenre());
        values.put(DIRECTOR, titleResult.getDirector());
        values.put(WRITER, titleResult.getWriter());
        values.put(ACTORS, titleResult.getActors());
        values.put(PLOT, titleResult.getPlot());
        values.put(LANGUAGE, titleResult.getLanguage());
        values.put(COUNTRY, titleResult.getCountry());
        values.put(AWARDS, titleResult.getAwards());
        values.put(POSTER, titleResult.getPoster());
        values.put(METASCORE, titleResult.getMetascore());
        values.put(IMDBRATING, titleResult.getImdbRating());
        values.put(IMDBVOTES, titleResult.getImdbVotes());
        values.put(IMDBID, titleResult.getImdbID());
        values.put(SERIESID, titleResult.getSeriesID());
        values.put(TYPE, titleResult.getType());
        values.put(TOTALSEASONS, titleResult.getTotalSeasons());
        values.put(UPDATEDAT, Long.parseLong(titleResult.getUpadatedAt()));
        values.put(VIEWEDAT, Long.parseLong(titleResult.getViewedAt()));

        // Inserting Row
        db.insert(TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    // Getting single contact
    public TitleIdResult getMovie(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{KEY_ID, TITLE, YEAR, RATED, RELEASED, SEASON, EPISODE, RUNTIME,
                        GENRE, DIRECTOR, WRITER, ACTORS, PLOT, LANGUAGE, COUNTRY, AWARDS, POSTER, METASCORE, IMDBRATING,
                        IMDBVOTES, IMDBID, SERIESID, TYPE, TOTALSEASONS, UPDATEDAT, VIEWEDAT
                }, IMDBID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            TitleIdResult titleIdResult = new TitleIdResult();
            cursor.getInt(0);
            titleIdResult.setTitle(cursor.getString(1));
            titleIdResult.setYear(cursor.getString(2));
            titleIdResult.setRated(cursor.getString(3));
            titleIdResult.setReleased(cursor.getString(4));
            titleIdResult.setSeason(cursor.getString(5));
            titleIdResult.setEpisode(cursor.getString(6));
            titleIdResult.setRuntime(cursor.getString(7));
            titleIdResult.setGenre(cursor.getString(8));
            titleIdResult.setDirector(cursor.getString(9));
            titleIdResult.setWriter(cursor.getString(10));
            titleIdResult.setActors(cursor.getString(11));
            titleIdResult.setPlot(cursor.getString(12));
            titleIdResult.setLanguage(cursor.getString(13));
            titleIdResult.setCountry(cursor.getString(14));
            titleIdResult.setAwards(cursor.getString(15));
            titleIdResult.setPoster(cursor.getString(16));
            titleIdResult.setMetascore(cursor.getString(17));
            titleIdResult.setImdbRating(cursor.getString(18));
            titleIdResult.setImdbVotes(cursor.getString(19));
            titleIdResult.setImdbID(cursor.getString(20));
            titleIdResult.setSeriesID(cursor.getString(21));
            titleIdResult.setType(cursor.getString(22));
            titleIdResult.setTotalSeasons(cursor.getString(23));
            titleIdResult.setUpadatedAt(String.valueOf(cursor.getLong(24)));
            titleIdResult.setViewedAt(String.valueOf(cursor.getLong(25)));
            return titleIdResult;
        }
        cursor.close();
        return null;
    }

    List<TitleIdResult> getMovieOffline(String search, String key, String type, String year, String page) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] array =  new String[]{KEY_ID, TITLE, YEAR, RATED, RELEASED, SEASON, EPISODE, RUNTIME,
                GENRE, DIRECTOR, WRITER, ACTORS, PLOT, LANGUAGE, COUNTRY, AWARDS, POSTER, METASCORE, IMDBRATING,
                IMDBVOTES, IMDBID, SERIESID, TYPE, TOTALSEASONS, UPDATEDAT, VIEWEDAT
        };
        List<TitleIdResult> titleIdResultList = new ArrayList<TitleIdResult>();
        ArrayList<String> typeSelected = new ArrayList<>();
        switch (type) {
            case "movie":
                //typeSelected.add("movie");
                break;
            case "series":
                //typeSelected.add("series");
                break;
            default:
                //typeSelected.add("series");
               // typeSelected.add("movie");
                break;
        }

        Cursor cursor;
        String query;
        switch (search) {
            case "s":
                key = "%" + key + "%";
                typeSelected.add(key);
                query = "SELECT * FROM " + TABLE_NAME
                        + " WHERE "
//                        + TYPE + " IN (" + makePlaceholders(typeSelected.size()) + ")"
//                        + " AND "
                        + TITLE + " LIKE ?" ;
                cursor = db.rawQuery(query, typeSelected.toArray(new String[typeSelected.size()]));
                break;
//            case "i":
//                cursor = db.query(TABLE_NAME, array, IMDBID + " = ?  AND " + TYPE + " IN ?",
//                        new String[]{String.valueOf(key), typeSelected}, null, null, null, null);
//                break;
//            case "t":
//                cursor = db.query(TABLE_NAME, array, TITLE + "= ? AND " + TYPE + " IN ?",
//                        new String[]{String.valueOf(key), typeSelected}, null, null, null, null);
//                break;
            default:
                key = "%" + key + "%";
                typeSelected.add(key);
                query = "SELECT * FROM " + TABLE_NAME
                        + " WHERE "
//                        + TYPE + " IN (" + makePlaceholders(typeSelected.size()) + ")"
//                        + " AND "
                            + TITLE + " LIKE ?" ;
                cursor = db.rawQuery(query, typeSelected.toArray(new String[typeSelected.size()]));
        }

        if (cursor != null && cursor.moveToFirst()) {
            do {
                TitleIdResult titleIdResult = new TitleIdResult();
                cursor.getInt(0);
                titleIdResult.setTitle(cursor.getString(1));
                titleIdResult.setYear(cursor.getString(2));
                titleIdResult.setRated(cursor.getString(3));
                titleIdResult.setReleased(cursor.getString(4));
                titleIdResult.setSeason(cursor.getString(5));
                titleIdResult.setEpisode(cursor.getString(6));
                titleIdResult.setRuntime(cursor.getString(7));
                titleIdResult.setGenre(cursor.getString(8));
                titleIdResult.setDirector(cursor.getString(9));
                titleIdResult.setWriter(cursor.getString(10));
                titleIdResult.setActors(cursor.getString(11));
                titleIdResult.setPlot(cursor.getString(12));
                titleIdResult.setLanguage(cursor.getString(13));
                titleIdResult.setCountry(cursor.getString(14));
                titleIdResult.setAwards(cursor.getString(15));
                titleIdResult.setPoster(cursor.getString(16));
                titleIdResult.setMetascore(cursor.getString(17));
                titleIdResult.setImdbRating(cursor.getString(18));
                titleIdResult.setImdbVotes(cursor.getString(19));
                titleIdResult.setImdbID(cursor.getString(20));
                titleIdResult.setSeriesID(cursor.getString(21));
                titleIdResult.setType(cursor.getString(22));
                titleIdResult.setTotalSeasons(cursor.getString(23));
                titleIdResult.setUpadatedAt(String.valueOf(cursor.getLong(24)));
                titleIdResult.setViewedAt(String.valueOf(cursor.getLong(25)));
                titleIdResultList.add(titleIdResult);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return titleIdResultList;
    }

    // Getting All Contacts
    public List<TitleIdResult> getAllresult() {
        List<TitleIdResult> titleIdResultList = new ArrayList<TitleIdResult>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TitleIdResult titleIdResult = new TitleIdResult();
                titleIdResult.setTitle(cursor.getString(1));
                titleIdResult.setYear(cursor.getString(2));
                titleIdResult.setRated(cursor.getString(3));
                titleIdResult.setReleased(cursor.getString(4));
                titleIdResult.setSeason(cursor.getString(5));
                titleIdResult.setEpisode(cursor.getString(6));
                titleIdResult.setRuntime(cursor.getString(7));
                titleIdResult.setGenre(cursor.getString(8));
                titleIdResult.setDirector(cursor.getString(9));
                titleIdResult.setWriter(cursor.getString(10));
                titleIdResult.setActors(cursor.getString(11));
                titleIdResult.setPlot(cursor.getString(12));
                titleIdResult.setLanguage(cursor.getString(13));
                titleIdResult.setCountry(cursor.getString(14));
                titleIdResult.setAwards(cursor.getString(15));
                titleIdResult.setPoster(cursor.getString(16));
                titleIdResult.setMetascore(cursor.getString(17));
                titleIdResult.setImdbRating(cursor.getString(18));
                titleIdResult.setImdbVotes(cursor.getString(19));
                titleIdResult.setImdbID(cursor.getString(20));
                titleIdResult.setSeriesID(cursor.getString(21));
                titleIdResult.setType(cursor.getString(22));
                titleIdResult.setTotalSeasons(cursor.getString(23));
                titleIdResult.setUpadatedAt(String.valueOf(cursor.getLong(24)));
                titleIdResult.setViewedAt(String.valueOf(cursor.getLong(25)));
                // Adding contact to list
                titleIdResultList.add(titleIdResult);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // return contact list
        return titleIdResultList;
    }

    // Updating single contact
    public int updateMovie(TitleIdResult titleResult) {
        SQLiteDatabase db = this.getWritableDatabase();
        titleResult.setUpadatedAt(String.valueOf(System.currentTimeMillis()));
        titleResult.setViewedAt(String.valueOf(System.currentTimeMillis()));
        ContentValues values = new ContentValues();
        values.put(TITLE, titleResult.getTitle());
        values.put(YEAR, titleResult.getYear());
        values.put(RATED, titleResult.getRated());
        values.put(RELEASED, titleResult.getReleased());
        values.put(SEASON, titleResult.getSeason());
        values.put(EPISODE, titleResult.getEpisode());
        values.put(RUNTIME, titleResult.getRuntime());
        values.put(GENRE, titleResult.getGenre());
        values.put(DIRECTOR, titleResult.getDirector());
        values.put(WRITER, titleResult.getWriter());
        values.put(ACTORS, titleResult.getActors());
        values.put(PLOT, titleResult.getPlot());
        values.put(LANGUAGE, titleResult.getLanguage());
        values.put(COUNTRY, titleResult.getCountry());
        values.put(AWARDS, titleResult.getAwards());
        values.put(POSTER, titleResult.getPoster());
        values.put(METASCORE, titleResult.getMetascore());
        values.put(IMDBRATING, titleResult.getImdbRating());
        values.put(IMDBVOTES, titleResult.getImdbVotes());
        values.put(SERIESID, titleResult.getSeriesID());
        values.put(TYPE, titleResult.getType());
        values.put(TOTALSEASONS, titleResult.getTotalSeasons());
        values.put(UPDATEDAT, Long.parseLong(titleResult.getUpadatedAt()));
        values.put(VIEWEDAT, Long.parseLong(titleResult.getViewedAt()));

        // updating row
        return db.update(TABLE_NAME, values, IMDBID + " = ?",
                new String[]{String.valueOf(titleResult.getImdbID())});
    }

    // Deleting single contact
    public void deleteContact(TitleIdResult titleIdResult) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, IMDBID + " = ?",
                new String[]{String.valueOf(titleIdResult.getImdbID())});
        db.close();
    }

    public void deleteViewedAt() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, " ? - " + VIEWEDAT + " > " + String.valueOf(((long) Constants.getTimeinMillisFromDay(7))),
                new String[]{String.valueOf(System.currentTimeMillis())});
        db.close();
    }


    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }
}
