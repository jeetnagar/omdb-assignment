package com.omdbapi.omdb.listeners;

public interface OnAddListener {
    void onItemAdd(int position);
}
