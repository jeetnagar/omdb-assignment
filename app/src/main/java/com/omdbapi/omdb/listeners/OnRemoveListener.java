package com.omdbapi.omdb.listeners;

public interface OnRemoveListener {
    void onItemRemoved(int position);
}
