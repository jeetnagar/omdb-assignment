package com.omdbapi.omdb.listeners;

public interface OnLoadMoreListener {
  void onLoadMore();
}