package com.omdbapi.omdb.modal.pojo;

import java.io.Serializable;

public class SearchResult implements Serializable
{
    private Search[] Search = new Search[1];

    private String totalResults = "";

    private String Response = "";

    private String Error = "noError";

    public Search[] getSearch ()
    {
        return Search;
    }

    public void setSearch (Search[] Search)
    {
        this.Search = Search;
    }

    public String getTotalResults ()
    {
        return totalResults;
    }

    public void setTotalResults (String totalResults)
    {
        this.totalResults = totalResults;
    }

    public String getResponse ()
    {
        return Response;
    }

    public void setResponse (String Response)
    {
        this.Response = Response;
    }

    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }
}