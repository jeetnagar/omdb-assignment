package com.omdbapi.omdb.modal;

import com.omdbapi.omdb.modal.pojo.Search;

import java.util.ArrayList;
import java.util.List;

public class SectionDataModel {



    private String headerTitle;
    private String totalCout;

    private String type;

    private List<Search> allItemsInSection;
    public SectionDataModel() {

    }

    public SectionDataModel(String headerTitle, String totalCout, String type, ArrayList<Search> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.totalCout = totalCout;
        this.type = type;
        this.allItemsInSection = allItemsInSection;
    }
    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public String getTotalCout() {
        return totalCout;
    }

    public void setTotalCout(String totalCout) {
        this.totalCout = totalCout;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Search> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(List<Search> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }


}