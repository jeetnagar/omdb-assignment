package com.omdbapi.omdb.modal.restclient;

import com.omdbapi.omdb.modal.pojo.SearchResult;
import com.omdbapi.omdb.modal.pojo.SeasonResult;
import com.omdbapi.omdb.modal.pojo.TitleIdResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET(".")
    Call<SearchResult> getSearchResult(@Query("s") String key, @Query("type") String type, @Query("y") String year, @Query("page") String page);

    @GET(".")
    Call<TitleIdResult> getTitleResult(@Query("t") String key, @Query("type") String type, @Query("y") String year, @Query("plot") String plot, @Query("tomatoes") String tomatoes);

    @GET(".")
    Call<SeasonResult> getTitleResult(@Query("t") String key, @Query("type") String type, @Query("y") String year, @Query("plot") String plot, @Query("tomatoes") String tomatoes, @Query("Season") String season);

    @GET(".")
    Call<TitleIdResult> getTitleResult(@Query("t") String key, @Query("type") String type, @Query("y") String year, @Query("plot") String plot, @Query("tomatoes") String tomatoes, @Query("Season") String season, @Query("Episode") boolean episode);

    @GET(".")
    Call<TitleIdResult> getIdResult(@Query("i") String key, @Query("type") String type, @Query("y") String year, @Query("plot") String plot, @Query("tomatoes") String tomatoes);
}
