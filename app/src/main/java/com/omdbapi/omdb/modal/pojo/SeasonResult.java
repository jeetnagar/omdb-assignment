package com.omdbapi.omdb.modal.pojo;

public class SeasonResult
{
    private String Response;

    private String Season;

    private Episodes[] Episodes;

    private String totalSeasons;

    private String Title;

    public String getResponse ()
    {
        return Response;
    }

    public void setResponse (String Response)
    {
        this.Response = Response;
    }

    public String getSeason ()
    {
        return Season;
    }

    public void setSeason (String Season)
    {
        this.Season = Season;
    }

    public Episodes[] getEpisodes ()
    {
        return Episodes;
    }

    public void setEpisodes (Episodes[] Episodes)
    {
        this.Episodes = Episodes;
    }

    public String getTotalSeasons ()
    {
        return totalSeasons;
    }

    public void setTotalSeasons (String totalSeasons)
    {
        this.totalSeasons = totalSeasons;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Response = "+Response+", Season = "+Season+", Episodes = "+Episodes+", totalSeasons = "+totalSeasons+", Title = "+Title+"]";
    }
}