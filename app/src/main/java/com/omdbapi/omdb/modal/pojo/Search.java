package com.omdbapi.omdb.modal.pojo;

import java.io.Serializable;

public class Search implements Serializable
{
    private String Year = "";

    private String Type = "";

    private String Poster = "";

    private String imdbID = "";

    private String Title = "";

    public Search(String year, String type, String poster, String imdbID, String title) {
        Year = year;
        Type = type;
        Poster = poster;
        this.imdbID = imdbID;
        Title = title;
    }

    public String getYear ()
    {
        return Year;
    }

    public void setYear (String Year)
    {
        this.Year = Year;
    }

    public String getType ()
    {
        return Type;
    }

    public void setType (String Type)
    {
        this.Type = Type;
    }

    public String getPoster ()
    {
        return Poster;
    }

    public void setPoster (String Poster)
    {
        this.Poster = Poster;
    }

    public String getImdbID ()
    {
        return imdbID;
    }

    public void setImdbID (String imdbID)
    {
        this.imdbID = imdbID;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }
}