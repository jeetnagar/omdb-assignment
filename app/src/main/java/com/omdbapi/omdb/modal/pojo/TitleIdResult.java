package com.omdbapi.omdb.modal.pojo;

import java.io.Serializable;

public class TitleIdResult implements Serializable
{
    private String Title = "";

    private String Year = "";

    private String Rated = "";

    private String Released = "";

    private String Season = "";

    private String Episode = "";

    private String Runtime = "";

    private String Genre = "";

    private String Director = "";

    private String Writer = "";

    private String Actors = "";

    private String Plot = "";

    private String Language = "";

    private String Country = "";

    private String Awards = "";

    private String Poster = "";

    private String Metascore = "";

    private String imdbRating = "";

    private String imdbVotes = "";

    private String imdbID = "";

    private String seriesID = "";

    private String Type = "";

    private String Response = "";

    private String Error = "noError";

    private String totalSeasons = "";

    private String UpadatedAt ="";

    private String ViewedAt = "";

    public String getViewedAt() {
        return ViewedAt;
    }

    public void setViewedAt(String viewedAt) {
        ViewedAt = viewedAt;
    }

    public String getUpadatedAt() {
        return UpadatedAt;
    }

    public void setUpadatedAt(String upadatedAt) {
        UpadatedAt = upadatedAt;
    }

    public String getReleased ()
    {
        return Released;
    }

    public String getType ()
    {
        return Type;
    }

    public String getImdbVotes ()
    {
        return imdbVotes;
    }

    public String getRuntime ()
    {
        return Runtime;
    }

    public String getResponse ()
    {
        return Response;
    }

    public String getPoster ()
    {
        return Poster;
    }

    public String getImdbID ()
    {
        return imdbID;
    }

    public String getCountry ()
    {
        return Country;
    }

    public String getTitle ()
    {
        return Title;
    }

    public String getImdbRating ()
    {
        return imdbRating;
    }

    public String getYear ()
    {
        return Year;
    }

    public String getRated ()
    {
        return Rated;
    }

    public String getActors ()
    {
        return Actors;
    }

    public String getPlot ()
    {
        return Plot;
    }

    public String getMetascore ()
    {
        return Metascore;
    }

    public String getWriter ()
    {
        return Writer;
    }

    public String getGenre ()
    {
        return Genre;
    }

    public String getLanguage ()
    {
        return Language;
    }

    public String getAwards ()
    {
        return Awards;
    }

    public String getSeason() {
        return Season;
    }

    public String getEpisode() {
        return Episode;
    }

    public String getSeriesID() {
        return seriesID;
    }

    public String getDirector ()
    {
        return Director;
    }

    public String getError() {
        return Error;
    }

    public String getTotalSeasons() {
        return totalSeasons;
    }

    public void setReleased (String Released)
    {
        this.Released = Released;
    }

    public void setType (String Type)
    {
        this.Type = Type;
    }

    public void setImdbVotes (String imdbVotes)
    {
        this.imdbVotes = imdbVotes;
    }

    public void setRuntime (String Runtime)
    {
        this.Runtime = Runtime;
    }

    public void setResponse (String Response)
    {
        this.Response = Response;
    }

    public void setPoster (String Poster)
    {
        this.Poster = Poster;
    }

    public void setImdbID (String imdbID)
    {
        this.imdbID = imdbID;
    }

    public void setCountry (String Country)
    {
        this.Country = Country;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    public void setImdbRating (String imdbRating)
    {
        this.imdbRating = imdbRating;
    }

    public void setYear (String Year)
    {
        this.Year = Year;
    }

    public void setRated (String Rated)
    {
        this.Rated = Rated;
    }

    public void setActors (String Actors)
    {
        this.Actors = Actors;
    }

    public void setPlot (String Plot)
    {
        this.Plot = Plot;
    }

    public void setMetascore (String Metascore)
    {
        this.Metascore = Metascore;
    }

    public void setWriter (String Writer)
    {
        this.Writer = Writer;
    }

    public void setGenre (String Genre)
    {
        this.Genre = Genre;
    }

    public void setLanguage (String Language)
    {
        this.Language = Language;
    }

    public void setAwards (String Awards)
    {
        this.Awards = Awards;
    }

    public void setDirector (String Director)
    {
        this.Director = Director;
    }

    public void setError(String error) {
        Error = error;
    }

    public void setTotalSeasons(String totalSeasons) {
        this.totalSeasons = totalSeasons;
    }

    public void setSeason(String season) {
        Season = season;
    }

    public void setEpisode(String episode) {
        Episode = episode;
    }

    public void setSeriesID(String seriesID) {
        this.seriesID = seriesID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Released = "+Released+", Type = "+Type+", imdbVotes = "+imdbVotes+", Runtime = "+Runtime+", Response = "+Response+", Poster = "+Poster+", imdbID = "+imdbID+", Country = "+Country+", Title = "+Title+", imdbRating = "+imdbRating+", Year = "+Year+", Rated = "+Rated+", Actors = "+Actors+", Plot = "+Plot+", Metascore = "+Metascore+", Writer = "+Writer+", Genre = "+Genre+", Language = "+Language+", Awards = "+Awards+", Director = "+Director+"]";
    }
}
