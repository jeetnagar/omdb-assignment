package com.omdbapi.omdb.modal.pojo;

public class Episodes
{
    private String imdbRating;

    private String Released;

    private String Episode;

    private String imdbID;

    private String Title;

    public String getImdbRating ()
    {
        return imdbRating;
    }

    public void setImdbRating (String imdbRating)
    {
        this.imdbRating = imdbRating;
    }

    public String getReleased ()
    {
        return Released;
    }

    public void setReleased (String Released)
    {
        this.Released = Released;
    }

    public String getEpisode ()
    {
        return Episode;
    }

    public void setEpisode (String Episode)
    {
        this.Episode = Episode;
    }

    public String getImdbID ()
    {
        return imdbID;
    }

    public void setImdbID (String imdbID)
    {
        this.imdbID = imdbID;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [imdbRating = "+imdbRating+", Released = "+Released+", Episode = "+Episode+", imdbID = "+imdbID+", Title = "+Title+"]";
    }
}